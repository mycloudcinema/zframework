const fs = require('fs');
const path = require('path');
const packageJsonFile = path.join(process.cwd(), '/package.json');

const simpleGit = require('simple-git');
const git = simpleGit();

let versionNumber;

// Increment the version number in the package.json file
try {

	let currentConfig = JSON.parse(fs.readFileSync(packageJsonFile, 'utf8'));
	let currentBuild = currentConfig.version.split('.');

	console.log(`Current version ${currentConfig.version}`);
	currentBuild[2]++;
	versionNumber = currentBuild.join('.');
	currentConfig.version = versionNumber;
	currentConfig._from = 'projectx-framework@' + versionNumber;
	console.log(`New version ${currentConfig.version}, ${currentConfig._from}`);

	fs.writeFileSync(packageJsonFile, JSON.stringify(currentConfig, null, 4));

} catch (error) {
	console.error('Could not locate the package file or the file is not valid JSON', packageJsonFile);
	console.error(error);
}

// Commit the changes to git and push
try {
	git.init()
		.then(function onInit (initResult) {
			console.log(initResult);
		})
		.then(()  => git.commit('Update the version number', ['package.json'], null, (commitDone) => {
			console.log(commitDone)
		}))
		.then(()  => git.addTag(versionNumber, (commitDone) => {
			console.log(commitDone)
		}))
		.then(() => git.push('origin', 'master', (pushDone) => {
			console.log(pushDone)
		}))
		.catch(err => console.error(err));

} catch (error) {
	console.error(error);
}
