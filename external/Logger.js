"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Log = exports.getLogger = void 0;
const ts_log_debug_1 = require("ts-log-debug");
const NO_USER_TAG = "-";
function getLogger(environment, settings) {
    return new Log(environment, settings);
}
exports.getLogger = getLogger;
class Log {
    constructor(environment, settings) {
        this.prefix = `[${environment}]			`;
        if (typeof settings === "undefined") {
            this.settings = {
                app: {
                    log_level: 3
                }
            };
        }
        else {
            this.settings = settings;
        }
    }
    trace(message, user) {
        if (this.settings.app.log_level < 4)
            return;
        ts_log_debug_1.$log.trace(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    }
    debug(message, user) {
        if (this.settings.app.log_level < 3)
            return;
        ts_log_debug_1.$log.debug(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    }
    info(message, user) {
        if (this.settings.app.log_level < 2)
            return;
        ts_log_debug_1.$log.info(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    }
    warn(message, user) {
        if (this.settings.app.log_level < 1)
            return;
        ts_log_debug_1.$log.warn(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    }
    error(message, user) {
        ts_log_debug_1.$log.error(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    }
}
exports.Log = Log;
