/*
╔════╗ ╔═══╗                             ╔╗
╚══╗ ║ ║╔══╝                             ║║
  ╔╝╔╝ ║╚══╗╔═╗╔══╗ ╔╗╔╗╔══╗╔╗╔╗╔╗╔══╗╔═╗║║╔╗
 ╔╝╔╝  ║╔══╝║╔╝╚ ╗║ ║╚╝║║╔╗║║╚╝╚╝║║╔╗║║╔╝║╚╝╝
╔╝ ╚═╗╔╝╚╗  ║║ ║╚╝╚╗║║║║║║═╣╚╗╔╗╔╝║╚╝║║║ ║╔╗╗
╚════╝╚══╝  ╚╝ ╚═══╝╚╩╩╝╚══╝ ╚╝╚╝ ╚══╝╚╝ ╚╝╚╝
*/
/**
 * Created by Forisz - April 4th 2015.
 * Current version maintained by My Cloud Cinema <mycloudcinema@remek.com>
 */

///<reference path='../tsc-descriptors/node/node.d.ts'/>

import * as path from 'path';
import * as pug from 'pug';

import {ITemplateManager} from "./TemplateManager";
import {Template} from "./TemplateManager";

export class EmailTemplateManager implements ITemplateManager {

	private templatesDirectory:string;
	private dictionaryData:any;

	constructor(basePath:string, dictionaryData:any) {
		if (dictionaryData !== null) {
			this.dictionaryData = dictionaryData;
		}
		this.templatesDirectory = basePath;
	}

	getDictionary(language:string):any {
		return 'asd';
	}

	getTemplate(templatePath:string, language:string, compress:boolean, data:any):Template {

		let source = '';

		source = pug.renderFile(path.join(this.templatesDirectory, templatePath), Object.assign({}, data, {
			dictionary: {
				CANCEL: 'Cancel'
			}
		}));

		let template:Template = new Template(language, source, compress);
		return template;
	}
}
