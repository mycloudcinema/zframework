/*
╔════╗ ╔═══╗                             ╔╗
╚══╗ ║ ║╔══╝                             ║║
  ╔╝╔╝ ║╚══╗╔═╗╔══╗ ╔╗╔╗╔══╗╔╗╔╗╔╗╔══╗╔═╗║║╔╗
 ╔╝╔╝  ║╔══╝║╔╝╚ ╗║ ║╚╝║║╔╗║║╚╝╚╝║║╔╗║║╔╝║╚╝╝
╔╝ ╚═╗╔╝╚╗  ║║ ║╚╝╚╗║║║║║║═╣╚╗╔╗╔╝║╚╝║║║ ║╔╗╗
╚════╝╚══╝  ╚╝ ╚═══╝╚╩╩╝╚══╝ ╚╝╚╝ ╚══╝╚╝ ╚╝╚╝
*/
/**
 * Created by Forisz - April 4th 2015.
 * Current version maintained by My Cloud Cinema <mycloudcinema@remek.com>
 */

///<reference path='../tsc-descriptors/node/node.d.ts'/>

export class Template {

	private _language: string;
	private _source: string;
	private size: number;
	private _compressed: boolean;

	get language():string {
		return this._language;
	}

	set language(value:string) {
		this._language = value;
	}

	get source():string {
		return this._source;
	}

	set source(value:string) {
		this._source = value;
		this.size = this._source.length;
	}

	get compressed():boolean {
		return this._compressed;
	}

	set compressed(value:boolean) {
		this._compressed = value;
	}

	constructor(language:string, source:string, compressed:boolean) {
		this._language = language;
		this._source = source;
		this._compressed = compressed;
		this.size = source.length; // TODO: non utf-8 characters have different size
	}
}

export interface ITemplateManager {
	getTemplate(path:string, language: string, compress: boolean, data:any): Template
}
