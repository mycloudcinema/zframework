"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Template = void 0;
class Template {
    constructor(language, source, compressed) {
        this._language = language;
        this._source = source;
        this._compressed = compressed;
        this.size = source.length;
    }
    get language() {
        return this._language;
    }
    set language(value) {
        this._language = value;
    }
    get source() {
        return this._source;
    }
    set source(value) {
        this._source = value;
        this.size = this._source.length;
    }
    get compressed() {
        return this._compressed;
    }
    set compressed(value) {
        this._compressed = value;
    }
}
exports.Template = Template;
