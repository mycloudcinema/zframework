"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailTemplateManager = void 0;
const path = require("path");
const pug = require("pug");
const TemplateManager_1 = require("./TemplateManager");
class EmailTemplateManager {
    constructor(basePath, dictionaryData) {
        if (dictionaryData !== null) {
            this.dictionaryData = dictionaryData;
        }
        this.templatesDirectory = basePath;
    }
    getDictionary(language) {
        return 'asd';
    }
    getTemplate(templatePath, language, compress, data) {
        let source = '';
        source = pug.renderFile(path.join(this.templatesDirectory, templatePath), Object.assign({}, data, {
            dictionary: {
                CANCEL: 'Cancel'
            }
        }));
        let template = new TemplateManager_1.Template(language, source, compress);
        return template;
    }
}
exports.EmailTemplateManager = EmailTemplateManager;
