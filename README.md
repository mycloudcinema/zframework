# ZFramework

A starter set for building a NodeJS powered website. The code doing the heavy lifting of building HTML Pages, JavaScript and CSS files. Language versioning and security is also handled here.

## Installation

To add base dependency to your application you can add the package using NPM:

npm install @mycloudcinema/zframework

### Contact Details ###

* My Cloud Cinema
* mycloudcinema@remek.com
