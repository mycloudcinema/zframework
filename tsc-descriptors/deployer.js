var Options = require('obfuscator').Options;
var obfuscator = require('obfuscator').obfuscator;
var fs = require('fs');
var options = new Options([
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/app.js',
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/factory/Dictionary.js',
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/factory/Request.js',
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/factory/Security.js',
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/factory/Session.js',
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/factory/Settings.js',
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/factory/Template.js',
    '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/factory/Utils.js'
], '/Users/Forisz/Git/zfor/zframework/ZFramework2/app/', 'app.js', true);

// custom compression options
// see https://github.com/mishoo/UglifyJS2/#compressor-options
options.compressor = {
    conditionals: true,
    evaluate: true,
    booleans: true,
    loops: true,
    unused: false,
    hoist_funs: false
};

obfuscator(options, function (err, obfuscated) {
    if (err) {
        throw err;
    }
    fs.writeFile('./dist.js', obfuscated, function (err) {
        if (err) {
            throw err;
        }
        console.log("Application deployed to ./dist.js");
    });
});