declare interface IPoloApp {
	name:string;
	heartbeat:number; // set the service heartbeat interval (defaults to 2min)
	port:number;
	multicast?:boolean; // disable/enable network multicast
	monitor?:boolean; // fork a monitor for faster failure detection,
}
declare interface IPolo {
	put(app:IPoloApp):void;
}
declare function polo():IPolo