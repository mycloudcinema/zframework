declare module xtend {
	interface XtendStatic {
		(a: Object, b:Object): Object;
	}
}
declare var xtend: xtend.XtendStatic;
declare module 'xtend' {
	export = xtend;
}
