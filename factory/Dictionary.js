"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DictionaryManager = exports.dictionaryPool = void 0;
const mysql = require("mysql");
const Settings = require("../external/Settings");
const Logger_1 = require("../external/Logger");
var settings = Settings;
const logger = Logger_1.getLogger("Dictionary Manager", settings);
exports.dictionaryPool = {};
class DictionaryManager {
    constructor() {
        this.loadDictionary = (callback) => {
            for (var i = 0; i < settings.template.supportedLanguages.length; i++) {
                exports.dictionaryPool[settings.template.supportedLanguages[i].toUpperCase()] = {};
            }
            var connection = this.createDatabaseConnection();
            logger.info(`Loading dictionary items for the following languages: ${settings.template.supportedLanguages}.`);
            for (var i = 0; i < settings.template.supportedLanguages.length; i++) {
                connection.query({
                    sql: "CALL get_dictionary_items(?,?)",
                    timeout: (settings.app.db_timeout || 30000),
                }, [
                    settings.template.supportedLanguages[i].toUpperCase(),
                    settings.app.site_name
                ], (error, rows, fields) => {
                    if (error) {
                        logger.error("There was an error while loading the dictionary items.", error);
                        if (typeof callback === "function") {
                            callback(true);
                        }
                    }
                    else {
                        var lang;
                        for (var j = 0; j < rows[0].length; j++) {
                            var item = rows[0][j];
                            exports.dictionaryPool[item.dictionary_language.toUpperCase()][item.dictionary_key] = item;
                            lang = item.dictionary_language.toUpperCase();
                        }
                        logger.debug(`${rows[0].length} dictionary items loaded for ${lang}.`);
                        if (lang === settings.template.supportedLanguages[settings.template.supportedLanguages.length - 1]) {
                            logger.info("Dictionary items loaded. Closing connection...");
                            connection.end();
                            logger.info("Dictionary connection closed.");
                            if (typeof callback === "function") {
                                callback(false);
                            }
                        }
                    }
                });
            }
        };
        this.getDictionaryItem = (lang, key) => {
            if (typeof lang === "string" && typeof key === "string")
                if (typeof exports.dictionaryPool[lang.toUpperCase()] !== "undefined" && typeof exports.dictionaryPool[lang.toUpperCase()][key.toUpperCase()] !== "undefined")
                    return exports.dictionaryPool[lang.toUpperCase()][key.toUpperCase()].dictionary_data;
            logger.warn(`Dictionary parse error. The requested item does not exist ${key}.`);
            return null;
        };
    }
    createDatabaseConnection() {
        logger.debug("Creating dictionary database connection.");
        try {
            if (!settings.dictionary.connection.connectTimeout) {
                settings.dictionary.connection.connectTimeout = settings.app.db_timeout || 10000;
            }
            if (!settings.dictionary.connection.timeout) {
                settings.dictionary.connection.timeout = settings.app.db_timeout || 30000;
            }
        }
        catch (ex) {
            logger.error(ex);
        }
        var connection = mysql.createConnection(settings.dictionary.connection);
        connection.connect();
        logger.debug(`Dictionary connection established ${connection.config.host}.`);
        return connection;
    }
}
exports.DictionaryManager = DictionaryManager;
