"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getConfigFilePath = exports.ConfigFileType = void 0;
const fs = require("fs-extra");
const settings = require("../external/Settings");
const Logger_1 = require("../external/Logger");
const logger = Logger_1.getLogger("Configuration Manager", settings);
const HOSTNAME = require('os').hostname();
let configurationData = new Map();
var ConfigFileType;
(function (ConfigFileType) {
    ConfigFileType[ConfigFileType["SCRIPT"] = 0] = "SCRIPT";
    ConfigFileType[ConfigFileType["STYLE"] = 1] = "STYLE";
    ConfigFileType[ConfigFileType["USER_DEFINED"] = 2] = "USER_DEFINED";
})(ConfigFileType = exports.ConfigFileType || (exports.ConfigFileType = {}));
class ConfigFile {
    constructor(data) {
        this._src = (src => {
            if (typeof src === 'string') {
                return src;
            }
            throw new Error('Invalid configuration json file. Missing or wrong field [src].');
        })(data.src);
        this._id = ((id) => {
            if (typeof id === 'string') {
                return id;
            }
            logger.warn(`Configuration file (${this._src}) has no identifier. To be able to use the configuration object with $config{} parsing you have to supply a an id.`);
            return null;
        })(data.id);
        this._type = (type => {
            if (typeof type === 'string') {
                switch (type.toLowerCase()) {
                    case "script":
                        return ConfigFileType.SCRIPT;
                    case "style":
                        return ConfigFileType.STYLE;
                    default:
                        return ConfigFileType.USER_DEFINED;
                }
            }
        })(data.type);
    }
    get src() {
        return this._src;
    }
    set src(v) {
        this._src = v;
    }
    get type() {
        return this._type;
    }
    set type(v) {
        this._type = v;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
}
class ConfigurationObject {
    constructor(data) {
        this._dependencies = (dependencies => {
            if (Array.isArray(dependencies)) {
                return dependencies;
            }
            return [];
        })(data.dependencies);
        this._files = (files => {
            if (Array.isArray(files)) {
                return files.map(file => new ConfigFile(file));
            }
            throw new Error('Invalid configuration object file. The configuration file list (files) property is not an array.');
        })(data.files);
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get dependencies() {
        return this._dependencies;
    }
    set dependencies(value) {
        this._dependencies = value;
    }
    get files() {
        return this._files;
    }
    set files(value) {
        this._files = value;
    }
}
function loadConfigObject() {
    let defaultFile = parseConfigFile('../data/config.json', null);
    if (defaultFile == null)
        throw new Error('There was an error loading the default configuration file');
    let systemSpecificFile = fs.existsSync(process.argv[4]) ? parseConfigFile(process.argv[4], {}) : parseConfigFile(`../data/config.${HOSTNAME}.json`, {});
    let combinedFile = deepExtend(defaultFile, systemSpecificFile);
    for (let key in combinedFile) {
        if (combinedFile.hasOwnProperty(key)) {
            configurationData.set(key, new ConfigurationObject(combinedFile[key]));
        }
    }
}
function parseConfigFile(filePath, defaultValue) {
    let tmp;
    try {
        logger.info(`Loading configuration file '${filePath}'.`);
        tmp = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    }
    catch (error) {
        logger.warn(`Unable and unwilling to load configuration files from file system '${filePath}'.`);
        return defaultValue;
    }
    return tmp;
}
function init() {
    loadConfigObject();
}
{
    init();
}
function getConfigFilePath(configurationObjectName) {
    let names = configurationObjectName.split('.');
    if (Array.isArray(names) && names.length === 2) {
        try {
            return configurationData.get(names[0]).files.find(file => file.id === names[1]).src;
        }
        catch (error) {
            logger.warn(`Could not locate configuration file with configuration object identifier ${configurationObjectName}.`);
            return null;
        }
    }
    return null;
}
exports.getConfigFilePath = getConfigFilePath;
function deepExtend() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i - 0] = arguments[_i];
    }
    var target = args[0];
    var val, src, clone;
    args.forEach(function (obj) {
        if (typeof obj !== 'object' || Array.isArray(obj)) {
            return;
        }
        Object.keys(obj).forEach(function (key) {
            src = target[key];
            val = obj[key];
            if (val === target) {
                return;
            }
            else if (typeof val !== 'object' || val === null) {
                target[key] = val;
                return;
            }
            else if (Array.isArray(val)) {
                target[key] = deepCloneArray(val);
                return;
            }
            else if (isSpecificValue(val)) {
                target[key] = cloneSpecificValue(val);
                return;
            }
            else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
                target[key] = deepExtend({}, val);
                return;
            }
            else {
                target[key] = deepExtend(src, val);
                return;
            }
        });
    });
    return target;
}
function deepCloneArray(arr) {
    var clone = [];
    arr.forEach(function (item, index) {
        if (typeof item === 'object' && item !== null) {
            if (Array.isArray(item)) {
                clone[index] = deepCloneArray(item);
            }
            else if (isSpecificValue(item)) {
                clone[index] = cloneSpecificValue(item);
            }
            else {
                clone[index] = deepExtend({}, item);
            }
        }
        else {
            clone[index] = item;
        }
    });
    return clone;
}
function isSpecificValue(val) {
    return !!(val instanceof Buffer
        || val instanceof Date
        || val instanceof RegExp);
}
function cloneSpecificValue(val) {
    if (val instanceof Buffer) {
        var x = new Buffer(val.length);
        val.copy(x);
        return x;
    }
    else if (val instanceof Date) {
        return new Date(val.getTime());
    }
    else if (val instanceof RegExp) {
        return new RegExp(val);
    }
    else {
        throw new Error('Unexpected situation');
    }
}
