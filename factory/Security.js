"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SecurityManager = void 0;
class SecurityManager {
    constructor() {
        this.checkLoggedIn = (sess) => {
            if (sess.logged_in)
                return true;
            return false;
        };
    }
}
exports.SecurityManager = SecurityManager;
