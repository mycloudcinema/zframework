/*
╔════╗ ╔═══╗                             ╔╗
╚══╗ ║ ║╔══╝                             ║║
  ╔╝╔╝ ║╚══╗╔═╗╔══╗ ╔╗╔╗╔══╗╔╗╔╗╔╗╔══╗╔═╗║║╔╗
 ╔╝╔╝  ║╔══╝║╔╝╚ ╗║ ║╚╝║║╔╗║║╚╝╚╝║║╔╗║║╔╝║╚╝╝
╔╝ ╚═╗╔╝╚╗  ║║ ║╚╝╚╗║║║║║║═╣╚╗╔╗╔╝║╚╝║║║ ║╔╗╗
╚════╝╚══╝  ╚╝ ╚═══╝╚╩╩╝╚══╝ ╚╝╚╝ ╚══╝╚╝ ╚╝╚╝
*/
/**
 * Created by Forisz - April 4th 2015.
 * Current version maintained by My Cloud Cinema <mycloudcinema@remek.com>
 */

import * as fs from 'fs-extra';
import * as settings from '../external/Settings';
import {getLogger} from "../external/Logger";

const logger = getLogger("Configuration Manager", settings);

/**
 * The name of the current host. The module will look for files with the following name: config.<hostname>.json
 * @type {any|string}
 */
const HOSTNAME = require('os').hostname();

let configurationData:Map<string, ConfigurationObject> = new Map();

export enum ConfigFileType {
	/**
	 * JavaScript file. Results in a <script> element
	 */
	SCRIPT,
	/**
	 * Cascading Style Sheet file. Results in a <link rel="stylesheet" /> element
	 */
	STYLE,
	/**
	 * User entered a custom type name
	 */
	USER_DEFINED
}

/**
 * Represents an actual file in the system.
 */
class ConfigFile {
	private _src:string;

	/**
	 * The source path of the file
	 * @returns {string}
	 */
	public get src() {
		return this._src
	}

	public set src(v) {
		this._src = v;
	}

	private _type:ConfigFileType;
	/**
	 * The type of the configuration file
	 * @returns {ConfigFileType}
	 */
	public get type() {
		return this._type;
	}

	public set type(v) {
		this._type = v;
	}

	private _name: string;

	/**
	 * The name of the file without extensions
	 */
	get name():string {
		return this._name;
	}

	set name(value:string) {
		this._name = value;
	}

	/**
	 * Unique configuration object identifier
	 */
	private _id:string;

	get id():string {
		return this._id;
	}

	set id(value:string) {
		this._id = value;
	}



	/**
	 * Data must be a parsed JSON object from any of the config json files.
	 * @param data
	 */
	constructor(data:any) {
		this._src = (src => {
			if (typeof src === 'string') {
				return src;
			}
			throw new Error('Invalid configuration json file. Missing or wrong field [src].');
		})(data.src);

		this._id = ((id) => {
			if (typeof id === 'string') {
				return id;
			}
			logger.warn(`Configuration file (${this._src}) has no identifier. To be able to use the configuration object with $config{} parsing you have to supply a an id.`);
			return null;
		})(data.id);

		this._type = (type => {
			if (typeof type === 'string') {
				switch (type.toLowerCase()) {
					case "script":
						return ConfigFileType.SCRIPT;
					case "style":
						return ConfigFileType.STYLE;
					default:
						return ConfigFileType.USER_DEFINED;
				}
			}
		})(data.type);
	}
}

/**
 * Represents a configuration object defined in a config json file.
 * A configuration object can contain several files
 */
class ConfigurationObject {
	/**
	 * The name of the configuration object
	 */
	private _name:string;
	/**
	 * List of configuration object dependencies
	 */
	private _dependencies:Array<string>;
	/**
	 * An array of configuration files of this configuration object
	 */
	private _files:Array<ConfigFile>;

	get name():string {
		return this._name;
	}

	set name(value:string) {
		this._name = value;
	}

	get dependencies():Array<string> {
		return this._dependencies;
	}

	set dependencies(value:Array<string>) {
		this._dependencies = value;
	}

	get files():Array<ConfigFile> {
		return this._files;
	}

	set files(value:Array<ConfigFile>) {
		this._files = value;
	}

	/**
	 * The data property must be a parsed json object of a config json file.
	 * @param data
	 */
	constructor(data:any) {
		// If the dependencies array is defined, then use it. Otherwise return an empty array.
		this._dependencies = (dependencies => {
			if (Array.isArray(dependencies)) {
				return dependencies;
			}
			return [];
		})(data.dependencies);

		this._files = (files => {
			if (Array.isArray(files)) {
				return files.map(file => new ConfigFile(file))
			}
			// By throwing an error we force the users to define at least one valid
			// file for each configuration. So we don't have to worry about them.
			throw new Error('Invalid configuration object file. The configuration file list (files) property is not an array.');
		})(data.files);

	}
}

/**
 * Load the configuration json files from the config directory. (default.json + <hostname>.json)
 * This will throw an error if the config.json file does not exist.
 */
function loadConfigObject() {

	let defaultFile = parseConfigFile('../data/config.json', null);
	if (defaultFile == null) throw new Error('There was an error loading the default configuration file');
	let systemSpecificFile = fs.existsSync(process.argv[4]) ? parseConfigFile(process.argv[4], {}) : parseConfigFile(`../data/config.${HOSTNAME}.json`, {});

	let combinedFile = deepExtend(defaultFile, systemSpecificFile);

	for (let key in combinedFile) {
		if (combinedFile.hasOwnProperty(key)) {
			configurationData.set(key, new ConfigurationObject(combinedFile[key]));
		}
	}

}

function parseConfigFile(filePath:string, defaultValue:any):any {
	let tmp;
	try {
		logger.info(`Loading configuration file '${filePath}'.`);
		tmp = JSON.parse(fs.readFileSync(filePath, 'utf8'));
	} catch (error) {
		logger.warn(`Unable and unwilling to load configuration files from file system '${filePath}'.`);
		return defaultValue;
	}
	return tmp;
}

function init() {
	loadConfigObject();
}

{
	init();
}

export function getConfigFilePath(configurationObjectName:string): string {
	let names = configurationObjectName.split('.');

	if (Array.isArray(names) && names.length === 2) {
		try {
			return configurationData.get(names[0]).files.find(file => file.id === names[1]).src;
		} catch(error) {
			logger.warn(`Could not locate configuration file with configuration object identifier ${configurationObjectName}.`);
			return null;
		}
	}
	return null;

}

/*
H E L P E R   F U N C T I O N S
*/

/**
 * Extening object that entered in first argument.
 *
 * Returns extended object or false if have no target object or incorrect type.
 *
 * If you wish to clone source object (without modify it), just use empty new
 * object as first argument, like this:
 *   deepExtend({}, yourObj_1, [yourObj_N]);
 */
function deepExtend() {

	var args = [];

	for (var _i = 0; _i < arguments.length; _i++) {
		args[_i - 0] = arguments[_i];
	}

	var target = args[0];
	var val, src, clone;

	args.forEach(function (obj) {

		if (typeof obj !== 'object' || Array.isArray(obj)) {
			return;
		}

		Object.keys(obj).forEach(function (key) {

			src = target[key];
			val = obj[key];

			if (val === target) {
				return;
			} else if (typeof val !== 'object' || val === null) {
				target[key] = val;
				return;
			} else if (Array.isArray(val)) {
				target[key] = deepCloneArray(val);
				return;
			} else if (isSpecificValue(val)) {
				target[key] = cloneSpecificValue(val);
				return;
			} else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
				target[key] = deepExtend({}, val);
				return;
			} else {
				target[key] = deepExtend(src, val);
				return;
			}

		});

	});

	return target;

}

function deepCloneArray(arr) {

	var clone = [];

	arr.forEach(function (item, index) {

		if (typeof item === 'object' && item !== null) {

			if (Array.isArray(item)) {
				clone[index] = deepCloneArray(item);
			} else if (isSpecificValue(item)) {
				clone[index] = cloneSpecificValue(item);
			} else {
				clone[index] = deepExtend({}, item);
			}
		} else {
			clone[index] = item;
		}

	});

	return clone;

}

function isSpecificValue(val) {
	return !!(val instanceof Buffer
		|| val instanceof Date
		|| val instanceof RegExp);
}

function cloneSpecificValue(val) {

	if (val instanceof Buffer) {
		var x = new Buffer(val.length);
		val.copy(x);
		return x;
	} else if (val instanceof Date) {
		return new Date(val.getTime());
	} else if (val instanceof RegExp) {
		return new RegExp(val);
	} else {
		throw new Error('Unexpected situation');
	}

}
