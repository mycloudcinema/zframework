"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.securityManager = exports.validateRequest = exports.SessionManager = void 0;
const mysql = require("mysql");
const Utils = require("./Utils");
const moment = require("moment");
const Settings = require("../external/Settings");
const Logger_1 = require("../external/Logger");
const crypto = require("crypto");
var settings = Settings;
var logger = Logger_1.getLogger("Session Manager", settings);
var log = new Utils.Logger("SessionManager", 0).log;
const ONE_YEAR = 365 * 24 * 60 * 60 * 1000;
const TWO_YEARS = 2 * ONE_YEAR;
const REMEMBER_COOKIE_NAME = "zfremember";
const DEFAULT_REMEMBER_COOKIE_LENGTH = 64;
const REMEMBER_COOKIE_LENGTH = settings.session.rememberCookieLength || DEFAULT_REMEMBER_COOKIE_LENGTH;
class SessionManager {
    constructor() {
        this.Login = (request, response, success, failure) => {
            try {
                logger.debug("Create login connection.");
                logger.debug(`User's IP address is ${request.ip_address}`);
                var connection = mysql.createConnection(settings.session.loginConnection);
                logger.debug("Login connection created.");
                connection.query("call get_user(?)", [request.body.username], (err, results) => {
                    logger.debug(`Loading data for user with login: ${request.body.username}`);
                    connection.end();
                    if (err) {
                        logger.error(`There was an error while loading user data.`, err);
                    }
                    else {
                        if (Object.prototype.toString.call(results) === '[object Array]') {
                            var query_details = results[1], query_response = results[0];
                            if (typeof query_response[0] !== "undefined") {
                                if (Utils.checkPasswordMatch(request.body.password, query_response[0].password)) {
                                    request.session.user = query_response[0];
                                    request.session.user.elevatedRights = {};
                                    request.session.user.session_data = {};
                                    var expires = moment().add(settings.session.expires, "minutes");
                                    request.session.user.session_expires = expires.toISOString();
                                    request.session.ip_address = request.ip;
                                    request.session.remember = this.handleRememberMe(request, response);
                                    success(query_response[0]);
                                }
                                else
                                    failure();
                            }
                            else
                                failure();
                        }
                        else
                            failure();
                    }
                });
            }
            catch (e) {
                logger.error("Failed to load user data.", e);
            }
        };
        this.grantElevatedRight = (request, action, success, failure) => {
            logger.debug("Create connection to the User's database.");
            var connection = mysql.createConnection(settings.session.loginConnection);
            connection.connect();
            connection.query("call get_user(?)", [request.body.username], function (error, results) {
                logger.debug(`Loading data for user with login: ${request.body.username}`);
                connection.end();
                if (Array.isArray(results)) {
                    var query_details = results[1], query_response = results[0];
                    if (typeof query_response !== "undefined") {
                        if (Utils.checkPasswordMatch(request.body.password, query_response[0].password)) {
                            var securityManager = new SecurityManager();
                            if (securityManager.hasAccessLite(query_response[0], action)) {
                                this.addElevatedRight(request.user, action);
                                success();
                                return;
                            }
                        }
                    }
                }
                failure();
            });
        };
        this.isLoggedIn = (request) => {
            logger.debug("Checking if user is logged in.", request.session.user);
            if (typeof request.session.user !== "undefined" && request.session.user.login) {
                logger.debug(`User session exists. Checking the remember me cookie (${REMEMBER_COOKIE_NAME}).`);
                if (request.session.remember && request.cookies[REMEMBER_COOKIE_NAME] && request.session.remember === request.cookies[REMEMBER_COOKIE_NAME]) {
                    let expires = moment().add(settings.session.expires, "minutes").toISOString();
                    request.session.user.session_expires = expires;
                    logger.debug(`Remember me cookie validated, extending session timeout to ${expires}.`);
                    return true;
                }
                else {
                    logger.debug("Remember me cookie does not exist. Continue regular session validation.");
                    if (request.ip === request.session.ip_address) {
                        logger.debug("IP address match. User identified.", request.session.user);
                        var difference = moment().diff(request.session.user.session_expires);
                        if (difference < 0) {
                            let expires = moment().add(settings.session.expires, "minutes");
                            request.session.user.session_expires = expires.toISOString();
                            logger.debug(`Session expires at ${request.session.user.session_expires}.`, request.session.user);
                            return true;
                        }
                        else
                            logger.debug("Session expired.", request.session.user);
                    }
                    else {
                        logger.warn(`The IP address from the request ${request.ip} does not match the user's saved IP ${request.session.ip_address}. Possible sesssion hijacking.`);
                        logger.info("Redirecting to the login page.");
                    }
                }
            }
            return false;
        };
        logger.debug(`Remember cookie length set to: ${REMEMBER_COOKIE_LENGTH} bytes (hexa).`);
    }
    handleRememberMe(request, response) {
        if (request.body.remember === 'true') {
            logger.debug(`The user selected the remember me option. ${JSON.stringify(request.body.remember)}`);
            const bytes = crypto.randomBytes(REMEMBER_COOKIE_LENGTH).toString('hex');
            response.cookie(REMEMBER_COOKIE_NAME, bytes, { maxAge: TWO_YEARS });
            return bytes;
        }
        return false;
    }
    setUserSessionData(request, data) {
        try {
            request.session.user.session_data = Object.assign(request.session.user.session_data, data);
        }
        catch (ex) {
            logger.error(ex);
        }
    }
    getUserSessionData(request, key) {
        try {
            if (key)
                return request.session.user.session_data[key];
            return request.session.user.session_data;
        }
        catch (ex) {
            return null;
        }
    }
    addElevatedRight(user, action) {
        if (user.elevatedRights.hasOwnProperty(action)) {
            user.elevatedRights[action].available = 1;
        }
        else {
            user.evevatedRights[action] = {
                available: 1
            };
        }
    }
}
exports.SessionManager = SessionManager;
class SecurityPool {
    constructor() {
        this.getFunctions = (user_role_id) => {
            if (this.pool.hasOwnProperty(user_role_id)) {
                return this.pool[user_role_id];
            }
            logger.debug(`User role ${user_role_id} does not exist in the security pool.`);
            return [];
        };
        this.setPool = (data) => {
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                if (this.pool.hasOwnProperty(item.user_role_id) && Array.isArray(this.pool[item.user_role_id])) {
                    this.pool[item.user_role_id].push(item.function_name);
                }
                else {
                    this.pool[item.user_role_id] = [item.function_name];
                }
            }
        };
        this.pool = {};
    }
}
class SecurityManager {
    constructor() {
        this.logger = Logger_1.getLogger("Security Manager", settings);
        this.securityPool = new SecurityPool();
        this.loadFunctions();
    }
    hasAccess(user, action) {
        if (settings.app.public_website) {
            this.logger.info(`Access granted for action: ${action} on a public website.`, user);
            return true;
        }
        if (settings.session.nonSecureItems.indexOf(action) !== -1) {
            this.logger.info(`Access granted for action: ${action}.`, user);
            return true;
        }
        this.logger.debug(`Checking whether the user has access to ${action}.`, user);
        if (settings.session.nonSecureItems.indexOf(action) !== -1) {
            this.logger.info(`Access granted for action: ${action}.`, user);
            return true;
        }
        var roles = JSON.parse(user.user_roles);
        for (var i = 0; i < roles.length; i++) {
            var functions = this.securityPool.getFunctions(roles[i]);
            if (functions.indexOf(action) !== -1) {
                this.logger.info(`Access granted for action: ${action}.`, user);
                return true;
            }
        }
        this.logger.warn(`Access denied for action ${action}`, user);
        return false;
    }
    hasAccessLite(user, action) {
        this.logger.debug(`Checking whether the user has access to ${action}.`, user);
        if (settings.app.public_website) {
            this.logger.info(`Access granted for action: ${action} on a public website.`, user);
            return true;
        }
        if (settings.session.nonSecureItems.indexOf(action) !== -1) {
            this.logger.info(`Access granted for action: ${action}.`, user);
            return true;
        }
        var roles = JSON.parse(user.user_roles);
        for (var i = 0; i < roles.length; i++) {
            var functions = this.securityPool.getFunctions(roles[i]);
            if (functions.indexOf(action) !== -1) {
                this.logger.info(`Access granted for action: ${action}.`, user);
                return true;
            }
        }
        this.logger.warn(`Access denied for action ${action}.`, user);
        return false;
    }
    reload() {
        this.securityPool = new SecurityPool();
        this.loadFunctions();
    }
    ;
    loadFunctions() {
        this.logger.info("Loading security information.");
        var connection = this.createSecurityConnection();
        connection.query({
            sql: "CALL get_security_userrolefunctions(?)",
            timeout: (settings.app.db_timeout || 30000),
        }, [settings.app.site_name], (error, rows, fields) => {
            this.logger.debug(`Security information loaded from the DB.`);
            if (error) {
                this.logger.error("There was an error while loading security information from the database.", error);
            }
            else {
                this.securityPool.setPool(rows[0]);
            }
            connection.end();
        });
    }
    createSecurityConnection() {
        try {
            if (!settings.security.connection.connectTimeout) {
                settings.security.connection.connectTimeout = settings.app.db_timeout || 10000;
            }
            if (!settings.security.connection.timeout) {
                settings.security.connection.timeout = settings.app.db_timeout || 30000;
            }
        }
        catch (ex) {
            logger.error(ex);
        }
        var connection = mysql.createConnection(settings.security.connection);
        connection.connect();
        logger.debug(`Security connection established ${connection.config.host}.`);
        return connection;
    }
}
function validateRequest(req, res) {
    logger.debug(`validating request`, { url: req.url, settings: settings.session.redirectIfMissing });
    if (Array.isArray(settings.session.redirectIfMissing) && settings.session.redirectIfMissing.length > 0) {
        if (settings.session.nonSecureItems.indexOf(req.url) === -1) {
            for (let i = 0; i < settings.session.redirectIfMissing.length; i++) {
                let item = settings.session.redirectIfMissing[i];
                if (!item.pages.some(page => isPageMatch(page, req.url))) {
                    continue;
                }
                if (Array.isArray(item.keys) && item.keys.length > 0) {
                    for (let j = 0; j < item.keys.length; j++) {
                        let key = item.keys[j];
                        try {
                            if (!req.session.user.session_data.hasOwnProperty(key)) {
                                logger.warn(`Session variable ${key} is missing. Redirecting to: ${item.to}`);
                                req.url = item.to;
                                return;
                            }
                            if (req.session.user.session_data[key] === null) {
                                logger.warn(`Session variable ${key} is missing. Redirecting to: ${item.to}`);
                                req.url = item.to;
                                return;
                            }
                        }
                        catch (ex) {
                            logger.error(ex);
                        }
                    }
                }
            }
        }
    }
}
exports.validateRequest = validateRequest;
function isPageMatch(page, url) {
    return !!url.match(new RegExp(page));
}
var s = new SecurityManager();
exports.securityManager = new SecurityManager();
