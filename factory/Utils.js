"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.copy = exports.checkPasswordMatch = exports.generateSalt = exports.Logger = exports.Severity = void 0;
const colors = require("colors");
const moment = require("moment");
const sjcl = require("sjcl-full");
var hostname = require("os").hostname();
var Severity;
(function (Severity) {
    Severity[Severity["log"] = 0] = "log";
    Severity[Severity["low"] = 1] = "low";
    Severity[Severity["medium"] = 2] = "medium";
    Severity[Severity["high"] = 3] = "high";
})(Severity = exports.Severity || (exports.Severity = {}));
class Logger {
    constructor(environment, level) {
        this.log = (message, severity) => {
            var logDate = moment();
            var messageData = "";
            if (Object.prototype.toString.call(message) == '[object Array]') {
                for (var i = 0; i < message.length; i++) {
                    if (Object.prototype.toString.call(message[i]) == '[object Object]') {
                        messageData += JSON.stringify(message[i]);
                    }
                    else {
                        messageData += message[i];
                    }
                }
            }
            else {
                if (Object.prototype.toString.call(message) == '[object Object]') {
                    messageData = JSON.stringify(message);
                }
                else {
                    messageData = message;
                }
            }
            console.log(logDate.format("YYYY-MM-DD HH:mm:ss.SSS") +
                " [" + this.environment + "] " +
                this.levels[severity ? severity : Severity.log](messageData));
        };
        this.environment = environment;
        this.levels = {};
        this.levels[Severity.log] = function (message) {
            return message;
        };
        this.levels[Severity.low] = function (message) {
            return colors.gray(message);
        };
        this.levels[Severity.medium] = function (message) {
            return colors.yellow(message);
        };
        this.levels[Severity.high] = function (message) {
            return colors.red(message);
        };
    }
}
exports.Logger = Logger;
function generateSalt(bytes) {
    var buffer = crypto.randomBytes(bytes), hexa = "";
    for (var i = 0; i < buffer.length; i++) {
        var byte = buffer[i].toString(16);
        if (byte.length === 1) {
            byte = "0" + byte;
        }
        hexa += byte;
    }
    return hexa;
}
exports.generateSalt = generateSalt;
function checkPasswordMatch(password, hash) {
    var salt = hash.substr(hash.length - 126), hashed_password = sjcl.codec.hex.fromBits(sjcl.hash.sha512.hash(password + salt));
    return ((hashed_password + salt) === hash);
}
exports.checkPasswordMatch = checkPasswordMatch;
;
function copy(obj) {
    return JSON.parse(JSON.stringify(obj));
}
exports.copy = copy;
