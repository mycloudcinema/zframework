/*
╔════╗ ╔═══╗                             ╔╗
╚══╗ ║ ║╔══╝                             ║║
  ╔╝╔╝ ║╚══╗╔═╗╔══╗ ╔╗╔╗╔══╗╔╗╔╗╔╗╔══╗╔═╗║║╔╗
 ╔╝╔╝  ║╔══╝║╔╝╚ ╗║ ║╚╝║║╔╗║║╚╝╚╝║║╔╗║║╔╝║╚╝╝
╔╝ ╚═╗╔╝╚╗  ║║ ║╚╝╚╗║║║║║║═╣╚╗╔╗╔╝║╚╝║║║ ║╔╗╗
╚════╝╚══╝  ╚╝ ╚═══╝╚╩╩╝╚══╝ ╚╝╚╝ ╚══╝╚╝ ╚╝╚╝
*/
/**
 * Created by Forisz - April 4th 2015.
 * Current version maintained by My Cloud Cinema <mycloudcinema@remek.com>
 */

// import {SessionManager} from "./factory/Session";
///<reference path='./tsc-descriptors/express.d.ts'/>
///<reference path='./tsc-descriptors/compression.d.ts'/>
///<reference path='./tsc-descriptors/mysql.d.ts'/>
///<reference path='./tsc-descriptors/express-session.d.ts'/>
///<reference path='./tsc-descriptors/serve-favicon.d.ts'/>
///<reference path='./tsc-descriptors/node/node.d.ts'/>
///<reference path='./tsc-descriptors/body-parser.d.ts'/>
///<reference path='./tsc-descriptors/moment.d.ts'/>
///<reference path='./tsc-descriptors/express-mysql-session.d.ts'/>
///<reference path='./tsc-descriptors/cookie-parser.d.ts'/>

const VERSION_NUMBER = '0.0.4';
const LANGUAGE_CHECK_REGEXP = /^\/(\w{2})($|\/)/;

import express = require("express");
import Utils = require("./factory/Utils");
import Request = require("./factory/Request");
import compress = require("compression");
// import mysql = require("mysql");
import favicon = require("serve-favicon");
import SESSION = require("express-session");
import PageManager = Request.PageManager;
import fs = require("fs-extra");
import bodyParser = require("body-parser");
import fwSession = require("./factory/Session");
import sessionStore = require("express-mysql-session");
import WebservicesManager = Request.WebservicesManager;
import moment = require("moment");
import Settings = require("./external/Settings");

import cors = require('cors');
import multer = require('multer');
import path = require("path");
import { v4 as uuidv4 } from 'uuid';

import URL = require("url");

import {getLogger} from "./external/Logger";

import * as cookieParser from "cookie-parser";

var settings:Settings.ISettings = Settings;

var log = new Utils.Logger("app", 0).log;

const logger = getLogger("Application index", settings);

// TODO: Move all non-static route settings to a separate node module (routes.js)

try {
	require('pmx').init({
		http:          true, // HTTP routes logging (default: true)
		ignore_routes: [], // Ignore http routes with this pattern (Default: [])
		errors:        true, // Exceptions loggin (default: true)
		custom_probes: true, // Auto expose JS Loop Latency and HTTP req/s as custom metrics
		network:       true, // Network monitoring at the application level
		ports:         false  // Shows which ports your app is listening on (default: false)
	});
} catch (error) {
	logger.error('Failed to load module pmx.', error);
}

module App {

	var port = settings.app.port_number;

	logger.info(`Starting ZFramework [Version ${VERSION_NUMBER}].`);

	init();

	var app = express();
	var pageManager = new PageManager(__dirname);
	var webserviceManager = new WebservicesManager(__dirname);
	var sessionManager = new fwSession.SessionManager();

	app.use(SESSION({
			name:              "NSID",
			secret:            "C'mon! This CAN'T be the self-destruct button. If it was, they wouldn' leave it lying around like this where anyone could push it! ",
			resave:            true,
			rolling:           true,
			saveUninitialized: true,
			cookie:            {secure: false, maxAge: false},
			expires:           "20m",
			store:             new sessionStore(settings.session.sessionStoreConnection)
		})
	);

	app.use(favicon(process.cwd() + "/../public/images/favicon.ico"));
	app.use(compress()); // Enables gzip compression

	// Enable cookie parsing for the framework
	app.use(cookieParser());

	//app.use(fwSession.validateRequest);

	function elapedTime(start) {
		const elapsed = process.hrtime(start); // divide by a million to get nano to milli
		return 1000 * elapsed[0] + elapsed[1] / 1000000;
	}

	app.use(function (req, res, next) {
		let start = process.hrtime();
		res.on('finish', function () {
			logger.debug(`Response generated in ${elapedTime(start)}ms for ${req.url}.`, '');
		});
		next();
	});

	// Allow the proxy to forward the remote IP address
	app.enable('trust proxy');

	app.use(bodyParser.json({limit: '128mb'}));
	app.use(bodyParser.urlencoded({extended: true, limit: '128mb'}));

	app.use("/public", express.static(process.cwd() + "/../public", {maxAge: 0/*604800000 *//*One week*/}));
	logger.info("Serving static resources at: /public");

	app.use("/media", express.static(settings.resource.mediaFolderPath));
	logger.info("Serving media at: " + settings.resource.mediaFolderPath);

	// File Upload Routes
	var upload = multer({
		storage: multer.memoryStorage()
	})

	app.use(cors());
	app.post('/upload', upload.array('files'), uploadRoute)

	function uploadRoute (req, res) {

		res.json({

			files: req.files.map(function (file) {

				console.log('Processing file', file);

				let uploadPath = settings.resource.mediaFolderPath + "/videos/";
				let fileName = uuidv4() + path.extname(file.originalname);

				console.log('Saving to', uploadPath + fileName);

				// Attempt to write a file to the file system. If this fails then we stop
				// processing an return a failure to the user.
				fs.mkdirs(uploadPath, function(err:any) {
					if (err) {
						console.error(err);
					} else {
						// fs.writeFileSync(uploadPath + fileName, file.buffer);
						fs.writeFile(uploadPath + fileName, file.buffer, function(err:any) {
							if (err) {
								console.error(err);
							} else {
								console.log('Upload', file);
								delete file.buffer
							}
						});
					}
				});
				return {
					uploadPath:	uploadPath,
					fileName:	fileName,
					file:		file
				}
			})

		})

	}

	// Webservices
	app.get("/webservices/*", webserviceManager.doGet);
	app.post("/webservices/*", webserviceManager.doPost);

	app.post("/elevate/webservices/*", function (request:Express.Request, response:Express.Response) {

		var new_url = request.url.replace("/elevate", "");
		var query = URL.parse(new_url);

		logger.debug(query);

		sessionManager.grantElevatedRight(request, query.pathname.replace("/webservices/", ""), function (elevationResponse) {
			logger.debug(elevationResponse);
			response.redirect(307, request.url.replace("/elevate", ""));
		}, function () {

		});

		// TODO: Check if the user that granted access has access to the called function.
		// TODO: If the thing passed security, then grant access to the requested function.

	});

	app.get("/session/get", function (request, response) {
		if (sessionManager.isLoggedIn(request)) {
			response.json({data: sessionManager.getUserSessionData(request), resultCode: 0});
		}
	});
	app.post("/session/set", function (request, response) {
		if (sessionManager.isLoggedIn(request)) {
			sessionManager.setUserSessionData(request, request.body);
			response.json({data: sessionManager.getUserSessionData(request), resultCode: 0})
		} else {
			response.json({data: "Are you logged in?", resultCode: 1});
		}
	});

	app.get("/logout", function (request, response) {
		request.session.destroy(function (error) {
			logger.info("Session destroyed.");
			response.redirect(settings.template.loginTemplateName || "/base/login");
		});
	});

	app.get('/*', function (request, response) {
		var parsedUrl = URL.parse(request.url);

		let match = parsedUrl.pathname.match(LANGUAGE_CHECK_REGEXP);

		if (match !== null) {
			request.url = request.url.replace(LANGUAGE_CHECK_REGEXP, '/');
			request.session.language = validateLanguage(match[1].toUpperCase());
		} else {
			if (typeof request.session.language === 'undefined') {
				request.session.language = settings.template.defaultLanguage;
			} else {
				request.session.language = validateLanguage(request.session.language);
			}
		}

		logger.info("Language set to " + request.session.language, request.session.user);

		pageManager.doGet(request, response);
	});
	//app.post('/', pageManager.doPost);

	app.post("/login", function (request, response) {
		sessionManager.Login(request, response, function (user) {
			var tmp;
			if (request.session.redirect_to) {
				tmp = request.session.redirect_to;
				delete request.session.redirect_to
			}
			// If the user request is just for a / then we will direct the user
			// to their default page.
			if (tmp === '/') {
				tmp = user.default_page;
			}
			logger.info(`User is now logged in.`, request.session.user);
			response.json({redirect: tmp ? tmp : user.default_page, resultCode: 0});
		}, function () {
			response.statusCode = 401;
			response.json({
				data:       "Login failed",
				resultCode: 1
			});
		})
	});

	const server = app.listen(port, function () {
		logger.info(`Application running [port ${port}].`);
	});

}

function init() {
	if (!fs.existsSync(process.cwd() + "/../public")) {
		log("[app.js] Could not locate public folder in application root.", Utils.Severity.medium);
		fs.mkdir(process.cwd() + "/../public");
		log("/public created");
		fs.mkdir(process.cwd() + "/../public/generated");
		log("/public/generated created");
	} else if (!fs.existsSync(process.cwd() + "/../public/generated")) {
		log("Could not locate folder for generated resources in public.", Utils.Severity.medium);
		fs.mkdir(process.cwd() + "/../public/generated");
		log("/public/generated created");
	}
}

// The user can pass a language through which is not valid for the site. In this
// case we need to replace their selected language with the site default for
// templates.
export function validateLanguage(language) {

	logger.debug(`Validating language ${language} against supported languages [${settings.template.supportedLanguages}].`);
	if (settings.template.supportedLanguages.indexOf(language) === -1) {
		logger.warn(`The selected language (${language}) is invalid or not supported. Switching to the default (${settings.template.defaultLanguage}).`);
		return settings.template.defaultLanguage;
	}
	return language
}
